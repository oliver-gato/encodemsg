PROJECTNAME := $(shell basename "$(PWD)")
VERSION=`git describe --tags`
BUILD := `git rev-parse HEAD`

GOCMD=go
GOBUILD=$(GOCMD) build
GOCLEAN=$(GOCMD) clean
GOTEST=$(GOCMD) test
GOGET=$(GOCMD) get
GOGENERATE=$(GOCMD) generate

all: help
##test:	Execute unit tests
test: 
	$(GOTEST) -race -v ./... -count=1
##bench:	Execute benchmark tests	
bench: 
	$(GOTEST) -v ./. -run=XXX -bench=. -benchmem -benchtime=3s
##lint:	Run linter, static check, error check, etc.
lint:
	golangci-lint run
##gen:	Run code generation
gen:
	$(GOGENERATE) ./...

#when building add a tag for a commit
#when running long duration test use another flag
#test-int integration and end-to-end tests also will have a separate flag

.PHONY: help test bench lint generate

##help:	Display help
help:	Makefile
	@echo
	@echo "Choose a command to run in "$(PROJECTNAME)":"
	@echo "Current version is" $(VERSION)
	@echo "build number:" $(BUILD)
	@echo
	@sed -n 's/^##//p' $< | column -t -s ':' |  sed -e 's/^/ /'
