//Package main is an app that performs message decoding
package main

import (
	"encodemsg/internal/crypto"
	"encodemsg/internal/privkey"
	"encoding/json"
	"flag"
	"fmt"
	"log"
)

func main() {
	sigStr, err := parseArgs()
	if err != nil {
		log.Fatal(err.Error())
	}
	privateKey := privkey.Value()
	decodePayload, err := crypto.DecodeMessage(sigStr, privateKey)
	if err != nil {
		log.Fatal(err.Error())
	}
	b, err := json.MarshalIndent(decodePayload, "", "	")
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println(string(b))
}

//ParseArgs processes input arguments for the decode application
func parseArgs() (
	string, //ciphertext
	error,
) {
	sigPtr := flag.String("cipher", "",
		"cipher text \nfor example: decodemsg -cipher \"asldkfjasdfhaskdjhfaskdjfhasdkjfhasdkjhf\"\n\n")
	flag.Parse()

	if flag.NArg() != 0 {
		flag.PrintDefaults()
		return "", fmt.Errorf("must specify only one argument after -cipher flag instead of an additional %d argument(s)", flag.NArg())
	}

	return *sigPtr, nil
}
