//Package main is an app that performs message encoding
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"

	"encodemsg/internal/crypto"
	"encodemsg/internal/pubkey"
)

func main() {
	msgBytes, err := parseArgs()
	if err != nil {
		log.Fatal(err.Error())
	}
	publicKey := pubkey.Value()
	encodePayload, err := crypto.EncodeMessage(msgBytes, publicKey)
	if err != nil {
		log.Fatal(err.Error())
	}
	b, err := json.MarshalIndent(encodePayload, "", "	")
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println(string(b))
}

//parseArgs processes input arguments for the encode application
func parseArgs() (
	[]byte, //input message
	error,
) {
	msgPtr := flag.String("message", "",
		"message string up to "+
			fmt.Sprint(crypto.MessageSize)+
			" bytes (required)\nfor example: encodemsg -message \"your@email.com\"\n\n")
	flag.Parse()

	if flag.NArg() != 0 {
		flag.PrintDefaults()
		return nil, fmt.Errorf("must specify only one argument after -message flag instead of an additional %d argument(s)", flag.NArg())
	}
	msgBytes := []byte(*msgPtr)

	return msgBytes, nil
}
