//Package main is an app that performs message signature verification.
//It accepts JSON output of message signing application as standard input.
package main

import (
	"encodemsg/internal/crypto"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"strings"
)

func main() {
	signPayload, err := readAllStdin()
	if err != nil {
		log.Fatal(err.Error())
	}
	//debug stdin
	// fmt.Printf("%+v \n", signPayload)
	// fmt.Println("")
	// fmt.Println("")

	verifyPayload, err := crypto.VerifySignature(signPayload)
	if err != nil {
		log.Fatal(err.Error())
	}
	b, err := json.MarshalIndent(verifyPayload, "", "	")
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println(string(b))
}

func readAllStdin() (
	crypto.SignPayload,
	error,
) {
	var sb strings.Builder
	var str string
	for {
		n, err := fmt.Scanf("%s", &str)
		if n == 0 || err == io.EOF {
			break
		}
		if err != nil {
			return crypto.SignPayload{}, err
		}
		//when strings are parsed, the spaces are removed so I must add them back to the string
		sb.WriteString(str)
		sb.WriteString(" ")
	}
	var signPayload crypto.SignPayload

	if err := json.Unmarshal([]byte(sb.String()), &signPayload); err != nil {
		return crypto.SignPayload{}, err
	}
	return signPayload, nil
}
