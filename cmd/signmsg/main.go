//Package main is an app that performs message signing
package main

import (
	"encodemsg/internal/crypto"
	"encodemsg/internal/privkey"
	"encodemsg/internal/pubkey"
	"encoding/json"
	"flag"
	"fmt"
	"log"
)

func main() {
	msgBytes, err := parseArgs()
	if err != nil {
		log.Fatal(err.Error())
	}
	signPayload, err := crypto.SignMessage(msgBytes, crypto.KeyPair{
		Public:  pubkey.Value(),
		Private: privkey.Value(),
	})
	if err != nil {
		log.Fatal(err.Error())
	}
	b, err := json.MarshalIndent(signPayload, "", "	")
	if err != nil {
		log.Fatal(err.Error())
	}

	fmt.Println(string(b))
}

//parseArgs processes input arguments for the message signing application
func parseArgs() (
	[]byte, //input message
	error,
) {
	msgPtr := flag.String("message", "",
		"message string up to "+
			fmt.Sprint(crypto.MessageSize)+
			" bytes (required)\nfor example: signmsg -message \"your@email.com\"\n\n")
	flag.Parse()

	if flag.NArg() != 0 {
		flag.PrintDefaults()
		return nil, fmt.Errorf("must specify no more arguments after all input flags instead of an additional %d argument(s)", flag.NArg())
	}
	msgBytes := []byte(*msgPtr)

	return msgBytes, nil
}
