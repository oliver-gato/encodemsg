# Container is based on a preexisting image that contains the Go tools needed
# to compile the source code
FROM golang:1.12 as builder

# Setup environment variables
ENV GOPATH /root/go/code
ENV PROJECT_DIR=${GOPATH}/src/
ENV GOBIN /root/go/bin
ENV PATH /root/go/bin:$PATH
ENV HOME /root

# Create project directory
RUN mkdir -p ${PROJECT_DIR}

# Change current working directory to project directory
WORKDIR ${PROJECT_DIR}

# Clone source code to project directory
RUN git clone https://bitbucket.org/oliver-gato/encodemsg.git

# Generate key. Once the container is created, the key cannot be changed.
# However, each time the container is built, new keys are generated
RUN go generate ./...

# this command will create a static build which is great for running in minimal containers
# http://blog.wrouesnel.com/articles/Totally%20static%20Go%20builds/

# build decoder; output shows packages that are in the executable to verify that we ONLY include private key
RUN CGO_ENABLED=0 GOOS=linux go install -v -a -ldflags '-extldflags "-static"' ${PROJECT_DIR}encodemsg/cmd/decodemsg

# build encoder; output shows packages that are in the executable to verify that we ONLY include public key
RUN CGO_ENABLED=0 GOOS=linux go install -v -a -ldflags '-extldflags "-static"' ${PROJECT_DIR}encodemsg/cmd/encodemsg

# build verify signature; output shows packages that are in the executable to verify that we ONLY include public key
RUN CGO_ENABLED=0 GOOS=linux go install -v -a -ldflags '-extldflags "-static"' ${PROJECT_DIR}encodemsg/cmd/verifymsg

# build signer; output shows packages that are in the executable to verify that we ONLY include public key
RUN CGO_ENABLED=0 GOOS=linux go install -v -a -ldflags '-extldflags "-static"' ${PROJECT_DIR}encodemsg/cmd/signmsg

# This image is created but not used in the next stage
# readme shows a command to stop the build here and apply a tag to this image using build cache!
# This is the decoder image
FROM scratch as decoder-stage
COPY --from=builder /root/go/bin/decodemsg .
ENTRYPOINT ["/decodemsg", "-cipher"]

# This is the encoder image
FROM scratch as encoder-stage
COPY --from=builder /root/go/bin/encodemsg .
ENTRYPOINT ["/encodemsg", "-message"]

# This is the verify signature image
FROM scratch as verify-stage
COPY --from=builder /root/go/bin/verifymsg .
ENTRYPOINT ["/verifymsg"]

# This is the signer image
FROM scratch
COPY --from=builder /root/go/bin/signmsg .
ENTRYPOINT ["/signmsg", "-message"]