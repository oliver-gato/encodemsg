//Package integration_test perform encoding and decoding of messages using applications in cmd directory
//as well as message signing using private key and signature verification.
//These two apps only contain private functions so this package serves as a unit test for that code.
//This test compiles the two apps and ensures that we can recover the encoded message.
package integration_test

import (
	"bytes"
	"encodemsg/internal/crypto"
	"encoding/json"
	"os/exec"
	"testing"
)

const msg = "Four score and seven years ago our fathers brought forth on this continent, a new nation, conceived in Liberty, and dedicated to the proposition that all men are created equal."

func TestShouldUseCmdEncodeDecode(t *testing.T) {
	//encode the message using command line utility
	cmd := exec.Command("go", "run", "../../cmd/encodemsg/main.go", "-message", msg)
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		t.Error(string(stdoutStderr))
		t.Error(err)
		return
	}
	encodedPayload := crypto.EncodePayload{}
	//unmarshal the json response of encode operation
	if err := json.Unmarshal(stdoutStderr, &encodedPayload); err != nil {
		t.Error(err)
		return
	}

	//decode the message using command line utility
	cmd2 := exec.Command("go", "run", "../../cmd/decodemsg/main.go", "-cipher", encodedPayload.Cipher)
	stdoutStderr2, err := cmd2.CombinedOutput()
	if err != nil {
		t.Error(string(stdoutStderr2))
		t.Error(err)
		return
	}
	decodedPayload := crypto.DecodePayload{}
	//unmarshal the json response of decode operation
	if err := json.Unmarshal(stdoutStderr2, &decodedPayload); err != nil {
		t.Error(err)
		return
	}

	if msg != decodedPayload.Message {
		t.Errorf("decoded message \n\n %s \n\n is not equal to the expected message \n\n %s", decodedPayload.Message, msg)
	}

}

func TestShouldUseCmdSignVerify(t *testing.T) {
	//sign the message using command line utility
	cmd := exec.Command("go", "run", "../../cmd/signmsg/main.go", "-message", msg)
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		t.Error(string(stdoutStderr))
		t.Error(err)
		return
	}
	//verify the signature using command line utility
	//the input to verify is a json payload so I do not have to Unmarshal like it is done in the other test here
	cmd2 := exec.Command("go", "run", "../../cmd/verifymsg/main.go")
	//send the bytes buffer which contains json payload to standard input of verify utility
	buffer := bytes.Buffer{}
	buffer.Write(stdoutStderr)
	cmd2.Stdin = &buffer
	//execute the verify message app
	stdoutStderr2, err := cmd2.CombinedOutput()
	if err != nil {
		t.Error(string(stdoutStderr2))
		t.Error(err)
		return
	}
	verifyPayload := crypto.VerifyPayload{}
	//unmarshal the json response of verify operation
	if err := json.Unmarshal(stdoutStderr2, &verifyPayload); err != nil {
		t.Error(err)
		return
	}

	if !verifyPayload.Verified {
		t.Errorf("signature verification failed with error: %s", verifyPayload.Error)
	}

}
