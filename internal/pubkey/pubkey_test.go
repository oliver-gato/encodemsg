package pubkey_test

import (
	"encodemsg/internal/crypto"
	"encodemsg/internal/pubkey"
	"testing"
)

func TestShouldVerifyKeys(t *testing.T) {
	if !crypto.IsValidPubKey([]byte(pubkey.Value())) {
		t.Error("invalid public key")
	}
}
