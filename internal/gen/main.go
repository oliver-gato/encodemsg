// Do not create an application during build by specifying this flag:

// +build ignore

// This utility generates public and private keys that are written as constants in their respective packages
package main

import (
	"encodemsg/internal/crypto"
	"encodemsg/internal/gen"
	"log"
	"time"
)

func main() {
	//create new pair of keys
	kp, err := crypto.MakeKeyPair()
	if err != nil {
		log.Fatal(err.Error())
	}
	//write two files with keys as go constants encoded in PEM format
	if err := createKeys(kp); err != nil {
		log.Fatal(err.Error())
	}

}

func createKeys(kp crypto.KeyPair) error {
	if err := gen.CreateFile(time.Now().UTC(), "../pubkey/rsa.go", "pubkey", "public", "pub", kp.Public); err != nil {
		return err
	}
	if err := gen.CreateFile(time.Now().UTC(), "../privkey/rsa.go", "privkey", "private", "priv", kp.Private); err != nil {
		return err
	}
	return nil
}
