//Package gen_test executes unit tests for code generation
package gen_test

import (
	"bytes"
	"encodemsg/internal/gen"
	"io/ioutil"
	"testing"
	"time"
)

const fakeKey = "-- This is not a real RSA key---"
const fakeFile = "./testdata/fakekey.txt"
const goldenFile = "./testdata/fakekey.golden"

func TestShouldCodeGen(t *testing.T) {
	timestamp := time.Time{}

	if err := gen.CreateFile(timestamp, fakeFile, "pubkey", "public", "pub", fakeKey); err != nil {
		t.Error(err)
	}

	actualBytes, err := ioutil.ReadFile(fakeFile)
	if err != nil {
		t.Error(err)
	}
	expectedBytes, err := ioutil.ReadFile(goldenFile)
	if err != nil {
		t.Error(err)
	}
	if !bytes.Equal(actualBytes, expectedBytes) {
		t.Errorf("generated file contents \n\n %s \n\n are not equal to expected file contents \n\n %s \n\n please run file diff on %s vs. %s",
			string(actualBytes), string(expectedBytes), fakeFile, goldenFile)
	}
}

func TestShouldError(t *testing.T) {
	timestamp := time.Time{}

	//passing in non-existing directory should result in file create error
	if err := gen.CreateFile(timestamp, "./fakedir/x.txt", "pubkey", "public", "pub", fakeKey); err == nil {
		t.Error("expected error from gen.CreateFile but got none")
	}

}
