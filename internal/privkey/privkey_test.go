package privkey_test

import (
	"encodemsg/internal/crypto"
	"encodemsg/internal/privkey"
	"testing"
)

func TestShouldVerifyKey(t *testing.T) {
	if !crypto.IsValidPrivKey([]byte(privkey.Value())) {
		t.Error("invalid private key")
	}
}
