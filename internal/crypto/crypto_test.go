package crypto_test

import (
	"bytes"
	"encodemsg/internal/crypto"
	"encodemsg/internal/privkey"
	"encodemsg/internal/pubkey"
	"errors"
	"fmt"
	"testing"
)

func helperInputMessages() []string {
	s := []string{
		"The mission, should you choose to accept it, is to encode/decode this message.",
		"I like my pancakes shaken not stirred",
		"Do we need sharks with lasers on their heads?",
	}
	return s
}
func helperEncodeDecode(pubkey, privkey, inputMessage string) error {

	encodedPayload, err := crypto.EncodeMessage([]byte(inputMessage), pubkey)
	if err != nil {
		return err
	}
	ciphertext := encodedPayload.Cipher

	decodePayload, err := crypto.DecodeMessage(ciphertext, privkey)
	if err != nil {
		return err
	}
	decodedMessage := decodePayload.Message
	if inputMessage != decodedMessage {
		return fmt.Errorf("Decoded message: \n\n %s \n\n is different than input message: \n\n %s", decodedMessage, inputMessage)
	}
	return nil
}

func helperSignVerify(pubkey, privkey, inputMessage string) error {
	signedPayload, err := crypto.SignMessage([]byte(inputMessage), crypto.KeyPair{
		Private: privkey,
		Public:  pubkey,
	})
	if err != nil {
		return err
	}
	verifyPayload, err := crypto.VerifySignature(signedPayload)
	if err != nil {
		return err
	}
	if !verifyPayload.Verified {
		return fmt.Errorf("%s", verifyPayload.Error)
	}
	return nil
}

func TestShouldVerifyKeys(t *testing.T) {
	if !crypto.IsValidPubKey([]byte(pubkey.Value())) {
		t.Error("invalid public key")
	}
	if !crypto.IsValidPrivKey([]byte(privkey.Value())) {
		t.Error("invalid private key")
	}

}
func TestShouldEncodeDecodeMessageUsingExistingKeys(t *testing.T) {
	//check if keys created by the go generate command are working ok with the message test cases
	for _, m := range helperInputMessages() {
		if err := helperEncodeDecode(pubkey.Value(), privkey.Value(), m); err != nil {
			t.Error(err.Error())
		}
	}
}

func TestShouldSignVerifyMessageUsingExistingKeys(t *testing.T) {
	for _, m := range helperInputMessages() {
		if err := helperSignVerify(pubkey.Value(), privkey.Value(), m); err != nil {
			t.Error(err.Error())
		}
	}
}
func TestShouldGenerateKeysEncodeDecodeMessage(t *testing.T) {
	//generate keys are re-run encryption and decryption test cases
	keyPair, err := crypto.MakeKeyPair()
	if err != nil {
		t.Error(err.Error())
	}
	for _, m := range helperInputMessages() {
		if err := helperEncodeDecode(keyPair.Public, keyPair.Private, m); err != nil {
			t.Error(err.Error())
		}
	}
}

func TestShouldTestMaxMessageSize(t *testing.T) {
	var buf bytes.Buffer
	for i := 0; i < crypto.MessageSize; i++ {
		buf.WriteString("X")
	}
	if err := helperEncodeDecode(pubkey.Value(), privkey.Value(), buf.String()); err != nil {
		t.Error(err)
	}
}

func TestShouldTestMaxMessageSizeSign(t *testing.T) {
	var buf bytes.Buffer
	for i := 0; i < crypto.MessageSize; i++ {
		buf.WriteString("X")
	}
	if err := helperSignVerify(pubkey.Value(), privkey.Value(), buf.String()); err != nil {
		t.Error(err)
	}
}

//in go 1.13, the failure test cases can now Unwrap the error and check that it is the correct one
func TestShouldFailMessageVerification(t *testing.T) {
	inputMessage := "You only live once, but if you do it right, once is enough."
	signedPayload, err := crypto.SignMessage([]byte(inputMessage), crypto.KeyPair{
		Private: privkey.Value(),
		Public:  pubkey.Value(),
	})
	if err != nil {
		t.Error(err)
	}
	signedPayload.Message = "You only live twice, but if you do it right, once is enough."
	verifyPayload, err := crypto.VerifySignature(signedPayload)
	if err != nil {
		t.Error(err)
	}
	//expecting verification failure because we changed the message
	if verifyPayload.Verified {
		t.Error("expected message verification error but got verified = true")
	}
}

func TestShouldFailMessageVerification2(t *testing.T) {
	inputMessage := "You only live once, but if you do it right, once is enough."
	signedPayload, err := crypto.SignMessage([]byte(inputMessage), crypto.KeyPair{
		Private: privkey.Value(),
		Public:  pubkey.Value(),
	})
	if err != nil {
		t.Error(err)
	}
	signedPayload.Message = ""
	//Most of the time it's a bad idea to expose public errors but it's ok to do that here since this is an internal package.
	if _, err := crypto.VerifySignature(signedPayload); !errors.Is(err, crypto.ErrEmptyMsgInVerifySignature) {
		t.Errorf("did not get expected error: %s but got: %s", crypto.ErrEmptyMsgInVerifySignature, err)
	}
}
func TestShouldFailMessageVerification3(t *testing.T) {
	inputMessage := "You only live once, but if you do it right, once is enough."
	signedPayload, err := crypto.SignMessage([]byte(inputMessage), crypto.KeyPair{
		Private: privkey.Value(),
		Public:  pubkey.Value(),
	})
	if err != nil {
		t.Error(err)
	}
	signedPayload.Pubkey = ""
	if _, err := crypto.VerifySignature(signedPayload); !errors.Is(err, crypto.ErrEmptyPubKeyInVerifySignature) {
		t.Errorf("did not get expected error: %s but got: %s", crypto.ErrEmptyPubKeyInVerifySignature, err)
	}
}
func TestShouldFailMessageVerification4(t *testing.T) {
	inputMessage := "You only live once, but if you do it right, once is enough."
	signedPayload, err := crypto.SignMessage([]byte(inputMessage), crypto.KeyPair{
		Private: privkey.Value(),
		Public:  pubkey.Value(),
	})
	if err != nil {
		t.Error(err)
	}
	signedPayload.Signature = ""
	if _, err := crypto.VerifySignature(signedPayload); !errors.Is(err, crypto.ErrEmptySignatureInVerifySignature) {
		t.Errorf("did not get expected error: %s but got: %s", crypto.ErrEmptySignatureInVerifySignature, err)
	}
}
func TestShouldFailMessageVerification5(t *testing.T) {
	inputMessage := "You only live once, but if you do it right, once is enough."
	signedPayload, err := crypto.SignMessage([]byte(inputMessage), crypto.KeyPair{
		Private: privkey.Value(),
		Public:  pubkey.Value(),
	})
	if err != nil {
		t.Error(err)
	}
	signedPayload.Signature = "XYZ"
	if _, err := crypto.VerifySignature(signedPayload); !errors.Is(err, crypto.ErrInDecodeSignatureInVerifySignature) {
		t.Errorf("did not get expected error: %s but got: %s", crypto.ErrInDecodeSignatureInVerifySignature, err)
	}
}

func TestShouldFailMessageVerification6(t *testing.T) {
	inputMessage := "You only live once, but if you do it right, once is enough."
	signedPayload, err := crypto.SignMessage([]byte(inputMessage), crypto.KeyPair{
		Private: privkey.Value(),
		Public:  pubkey.Value(),
	})
	if err != nil {
		t.Error(err)
	}
	signedPayload.Pubkey = "XYZ"
	if _, err := crypto.VerifySignature(signedPayload); !errors.Is(err, crypto.ErrInDecodePubKeyInVerifySignature) {
		t.Errorf("did not get expected error: %s but got: %s", crypto.ErrInDecodePubKeyInVerifySignature, err)
	}

}
func TestShouldFailMessageVerification7(t *testing.T) {
	inputMessage := "You only live once, but if you do it right, once is enough."
	signedPayload, err := crypto.SignMessage([]byte(inputMessage), crypto.KeyPair{
		Private: privkey.Value(),
		Public:  pubkey.Value(),
	})
	if err != nil {
		t.Error(err)
	}
	var buf bytes.Buffer
	for i := 0; i <= crypto.MessageSize; i++ {
		buf.WriteString("X")
	}
	signedPayload.Message = buf.String()
	if _, err := crypto.VerifySignature(signedPayload); !errors.Is(err, crypto.ErrMsgTooLargeInVerifySignature) {
		t.Errorf("did not get expected error: %s but got: %s", crypto.ErrMsgTooLargeInVerifySignature, err)
	}

}
func TestShouldFailEmptyMessage(t *testing.T) {
	if err := helperEncodeDecode(pubkey.Value(), privkey.Value(), ""); !errors.Is(err, crypto.ErrEmptyMsgInEncodeMessage) {
		t.Errorf("did not get expected error: %s but got: %s", crypto.ErrEmptyMsgInEncodeMessage, err)
	}
}
func TestShouldFailEmptyMessageSign(t *testing.T) {
	if err := helperSignVerify(pubkey.Value(), privkey.Value(), ""); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailTooLongMessage(t *testing.T) {
	var buf bytes.Buffer
	for i := 0; i <= crypto.MessageSize; i++ {
		buf.WriteString("X")
	}
	if err := helperEncodeDecode(pubkey.Value(), privkey.Value(), buf.String()); err == nil {
		t.Error("expected non empty error but got none")
	}
}
func TestShouldFailTooLongMessageSign(t *testing.T) {
	var buf bytes.Buffer
	for i := 0; i <= crypto.MessageSize; i++ {
		buf.WriteString("X")
	}
	if err := helperSignVerify(pubkey.Value(), privkey.Value(), buf.String()); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailEmptyPubKey(t *testing.T) {
	if err := helperEncodeDecode("", privkey.Value(), "X"); err == nil {
		t.Error("expected non empty error but got none")
	}
}
func TestShouldFailEmptyPubKeySign(t *testing.T) {
	if err := helperSignVerify("", privkey.Value(), "X"); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailInvalidPubKey(t *testing.T) {
	if err := helperEncodeDecode("WRONG PUB KEY", privkey.Value(), "X"); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailEmptyPrivKey(t *testing.T) {
	if err := helperEncodeDecode(pubkey.Value(), "", "X"); err == nil {
		t.Error("expected non empty error but got none")
	}
}
func TestShouldFailEmptyPrivKeySign(t *testing.T) {
	if err := helperSignVerify(pubkey.Value(), "", "X"); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailInvalidPrivKey(t *testing.T) {
	if err := helperEncodeDecode(pubkey.Value(), "WRONG PRIV KEY", "X"); err == nil {
		t.Error("expected non empty error but got none")
	}
}
func TestShouldFailInvalidPrivKeySign(t *testing.T) {
	if err := helperSignVerify(pubkey.Value(), "WRONG PRIV KEY", "X"); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailEmptyCipher(t *testing.T) {
	if _, err := crypto.DecodeMessage("", privkey.Value()); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailInvalidCipher(t *testing.T) {
	if _, err := crypto.DecodeMessage("Xyz", privkey.Value()); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailInvalidPubKey2(t *testing.T) {
	if crypto.IsValidPubKey([]byte("xyz")) {
		t.Error("expected invalid pub key")
	}
}

func TestShouldFailInvalidPrivKey2(t *testing.T) {
	if crypto.IsValidPrivKey([]byte("xyz")) {
		t.Error("expected invalid priv key")
	}
}

const okPriv = `-----BEGIN RSA PRIVATE KEY-----
MIGsAgEAAiEA3NdN6syvGk0fm2GXGkYLqS2ZlZtCrfZlNf+3xPvg7aMCAwEAAQIh
AJF1Bo8mrhamwbC4QKw/gqF7wLPgWM5UXxRgvweyzPChAhEA3gl5TPb0u6YqxJCT
qEE7uwIRAP6e/6GRs1bfZZEgYbinUzkCEBX5bg3V6wafPWBSxqpCOn0CEQCIvuGx
Zyu240GMAKUa45pZAhEAuiknpgy6lGvLO4gkA6VRzQ==
-----END RSA PRIVATE KEY-----
`

const okPub = `-----BEGIN RSA PUBLIC KEY-----
MDwwDQYJKoZIhvcNAQEBBQADKwAwKAIhANzXTerMrxpNH5thlxpGC6ktmZWbQq32
ZTX/t8T74O2jAgMBAAE=
-----END RSA PUBLIC KEY-----
`

func TestShouldFailMessageTooLongForKeySize(t *testing.T) {
	if err := helperEncodeDecode(okPub, okPriv, "X"); err == nil {
		t.Error("expected non empty error but got none")
	}
}

func TestShouldFailDecode2(t *testing.T) {
	//the cipher is valid and the key is valid but the cipher was created with different public key
	if _, err := crypto.DecodeMessage("X9prxMN7PObi6as/FTNhHhgYguJidJgQebuCs7bkBQGv1VPF0OAYGPOba6G1Lk392+m1C9NBx4XSciGxTaJ6Xufddzj/TypYhM9TTOwvFUjQVledI2vB7XzPF/S/jzNK428Yt52PTLyfWkJIiHlB/HSnaZiym7u9iCWYDoPUCakCNMePIX0U5pmoIe5nSSXXBnG4kCI8fgOoBhEQ36ThXHEYwMy+a3OIABrcUXCG2hTDkmv2guUDo65pkGcKMmoahu1DYk2lCu5hUW9XGOhh4krqTp3pggmQMvpvkI5sXNJVPenhdV4+ujaWQNbCTiIJX5sX0l9LWY4J+lNyoyw2hE+HrDzU2JR+X4ONOwUn+pzjiCHQXRBlTCiSiSCZRP9d2JAjnUQue2iOB3XYifqLG8n8nATrTLwmXtEi8yK6xA64uJrGgOo3fCaHzQeQOIxw3Apy/V6JoM/H1S7/lEnc9MLVuOhBjCfi0IerNHa/QHMn2Fz2myMsT1sVZgGKVAe82/W09J+Oirk6l9q6HNjPWsqAg0M5LTRyzaIWWA4+uIJAZX+Vy4jAGvEDjlEV4+YFCwPwc/r2JpvEPcwOV88y2ZxXCom3R2g54WrallTN5ZYjEgEcX0UxFtoJDcHtdLRXPAN/biRfJ/RJv7AiGWI6ZkC2tPiMNs+dk2nBz+ETWq8=",
		okPriv); err == nil {
		t.Error("expected non empty error but got none")
	}
}
