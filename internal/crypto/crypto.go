//Package crypto has functions that perform cryptographic encoding, decoding, signinig and verifying using RSA algorithm
package crypto

import (
	"crypto"
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"fmt"
)

//erratum is a type that is used to implement constant errors
//RAD NOTES: provide constant sentinel errors for PUBLIC functions of INTERNAL packages ONLY!
//This will let me write tests using the specific error values with !errors.Is(expected, actual)
type erratum string

//Error function fullfils the error interface for erratum type
func (e erratum) Error() string { return string(e) }

//ErrEmptyMsgInVerifySignature is a constant error
//It is a bad idea to expose error codes externally because consumers of package might rely on it
//and you will not be able to change them.  However, since this package it's internal, the external clients
//will NOT see these errors.
//Another idea is to to check errors for behavior and not type:
//https://dave.cheney.net/2014/12/24/inspecting-errors
const (
	ErrEmptyMsgInVerifySignature          = erratum("must specify non empty message for signature verification")
	ErrEmptyPubKeyInVerifySignature       = erratum("public key is empty, please execute command: go generate")
	ErrEmptySignatureInVerifySignature    = erratum("must provide non empty signature string for verification")
	ErrMsgTooLargeInVerifySignature       = erratum("message too large")
	ErrInDecodeSignatureInVerifySignature = erratum("could not decode signature")
	ErrInDecodePubKeyInVerifySignature    = erratum("could not decode public key")
	ErrEmptyMsgInEncodeMessage            = erratum("must specify non empty message")
)

//RSABits specifies RSA key size;
//must be at least ( max message bytes + 11 ) * 8
const RSABits = 4096

//MessageSize is the maximum message length in characters that is accepted as valid input
const MessageSize = 250

//KeyPair has a public/private key pair PEM encoded and converted to byte slices
type KeyPair struct {
	Private string
	Public  string
}

//SignPayload contains output of signing the message using private key
type SignPayload struct {
	Message   string `json:"message"`
	Signature string `json:"signature"`
	Pubkey    string `json:"pubkey"`
}

//VerifyPayload contains output of the message signature verification using public key
type VerifyPayload struct {
	Verified bool   `json:"verified"`
	Error    string `json:"error,omitempty"`
}

//DecodePayload contains output of the decoding applied to the input string
type DecodePayload struct {
	Message string `json:"message"`
}

//EncodePayload contains output of the encoding applied to the input string
type EncodePayload struct {
	Cipher string `json:"cipher"`
	Pubkey string `json:"pubkey"`
}

//SignMessage returns a message signed with private key; the response payload contains
func SignMessage(msgBytes []byte, kp KeyPair) (
	SignPayload,
	error,
) {
	if len(msgBytes) == 0 {
		return SignPayload{}, errors.New("must specify non empty message for signing")
	}
	if len(msgBytes) > MessageSize {
		return SignPayload{}, fmt.Errorf("message size is %d bytes which exceeds maximum size of %d bytes", len(msgBytes), MessageSize)
	}
	if len(kp.Private) == 0 {
		return SignPayload{}, errors.New("private key is empty, please execute command: go generate")
	}
	if len(kp.Public) == 0 {
		return SignPayload{}, errors.New("public key is empty, please execute command: go generate")
	}

	rsaPrivate, err := bytesToPrivateKey([]byte(kp.Private))
	if err != nil {
		return SignPayload{}, err
	}

	signature, err := signWithPrivateKey(msgBytes, rsaPrivate)
	if err != nil {
		return SignPayload{}, err
	}
	signPayload := SignPayload{
		Message:   string(msgBytes),
		Signature: base64.StdEncoding.EncodeToString(signature),
		Pubkey:    kp.Public,
	}
	return signPayload, nil
}

//VerifySignature returns a signature verification outcome;
//externalPubKey is not from the internal package, rather it's taken from the SignPayload response
func VerifySignature(
	signPayload SignPayload,
) (
	VerifyPayload,
	error,
) {
	msgBytes := []byte(signPayload.Message)

	if len(msgBytes) == 0 {
		return VerifyPayload{}, ErrEmptyMsgInVerifySignature
	}
	if len(msgBytes) > MessageSize {
		return VerifyPayload{}, fmt.Errorf("message size is %d bytes which exceeds maximum size of %d bytes: %w", len(msgBytes), MessageSize, ErrMsgTooLargeInVerifySignature)
	}
	if len(signPayload.Pubkey) == 0 {
		return VerifyPayload{}, ErrEmptyPubKeyInVerifySignature
	}
	if len(signPayload.Signature) == 0 {
		return VerifyPayload{}, ErrEmptySignatureInVerifySignature
	}

	signature, err := base64.StdEncoding.DecodeString(signPayload.Signature)
	if err != nil {
		return VerifyPayload{}, fmt.Errorf("error %v while base64.StdEncoding.DecodeString in VerifySignature: %w", err, ErrInDecodeSignatureInVerifySignature)
	}
	rsaPublic, err := bytesToPublicKey([]byte(signPayload.Pubkey))
	if err != nil {
		return VerifyPayload{}, fmt.Errorf("error %v while bytesToPublicKey in VerifySignature: %w", err, ErrInDecodePubKeyInVerifySignature)
	}
	if err := verifySignatureWithExternalPublicKey(msgBytes, signature, rsaPublic); err != nil {
		return VerifyPayload{
			Verified: false,
			Error:    err.Error(),
		}, nil
	}
	// if there were no errors during verification then return true bool flag
	return VerifyPayload{Verified: true}, nil
}

//DecodeMessage returns correctly decoded message
func DecodeMessage(encodedMsg, privateKey string) (
	DecodePayload,
	error,
) {
	if len(encodedMsg) == 0 {
		return DecodePayload{}, errors.New("must specify non empty encoded message for decoding")
	}
	if len(privateKey) == 0 {
		return DecodePayload{}, errors.New("private key is empty, please execute command: go generate")
	}
	ciphertext, err := base64.StdEncoding.DecodeString(encodedMsg)
	if err != nil {
		return DecodePayload{}, fmt.Errorf("while base64.StdEncoding.DecodeString in DecodeMessage: %w", err)
	}
	rsaPrivate, err := bytesToPrivateKey([]byte(privateKey))
	if err != nil {
		return DecodePayload{}, err
	}
	msgBytes, err := decryptWithPrivateKey(ciphertext, rsaPrivate)
	if err != nil {
		return DecodePayload{}, err
	}
	decodePayload := DecodePayload{
		Message: string(msgBytes),
	}
	return decodePayload, nil
}

//EncodeMessage returns correctly encoded message
func EncodeMessage(msgBytes []byte, publicKey string) (
	EncodePayload,
	error,
) {

	if len(msgBytes) == 0 {
		return EncodePayload{}, ErrEmptyMsgInEncodeMessage
	}

	if len(msgBytes) > MessageSize {
		return EncodePayload{}, fmt.Errorf("message size is %d bytes which exceeds maximum size of %d bytes", len(msgBytes), MessageSize)
	}

	if len(publicKey) == 0 {
		return EncodePayload{}, errors.New("public key is empty, please execute command: go generate")
	}
	rsaPublic, err := bytesToPublicKey([]byte(publicKey))
	if err != nil {
		return EncodePayload{}, err
	}
	ciphertext, err := encryptWithPublicKey(msgBytes, rsaPublic)
	if err != nil {
		return EncodePayload{}, err
	}
	encodePayload := EncodePayload{
		Cipher: base64.StdEncoding.EncodeToString(ciphertext),
		Pubkey: publicKey,
	}
	return encodePayload, nil
}

// MakeKeyPair creates a pair of public and private keys which are PEM encoded.
func MakeKeyPair() (
	KeyPair,
	error,
) {
	//create private key
	privateKey, err := rsa.GenerateKey(rand.Reader, RSABits)

	if err != nil {
		return KeyPair{}, fmt.Errorf("while rsa.GenerateKey in MakeKeyPair: %w", err)
	}

	// generate and write private key as PEM
	privateKeyPEM := &pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(privateKey)}
	//encode private key PEM to bytes
	privateBytes := pem.EncodeToMemory(privateKeyPEM)
	//extract public key from private key
	pubBytes, err := x509.MarshalPKIXPublicKey(&privateKey.PublicKey)
	if err != nil {
		return KeyPair{}, fmt.Errorf("while x509.MarshalPKIXPublicKey in MakeKeyPair: %w", err)
	}
	//write public key as PEM
	publicKeyPEM := &pem.Block{Type: "RSA PUBLIC KEY", Bytes: pubBytes}
	//encode public key PEM as bytes
	publicBytes := pem.EncodeToMemory(publicKeyPEM)

	kp := KeyPair{
		Private: string(privateBytes),
		Public:  string(publicBytes),
	}
	return kp, nil
}

//IsValidPubKey verifies that public key is valid
func IsValidPubKey(pub []byte) bool {
	if _, err := bytesToPublicKey(pub); err != nil {
		return false
	}
	return true
}

//IsValidPrivKey verifies that public key is valid
func IsValidPrivKey(pub []byte) bool {
	if _, err := bytesToPrivateKey(pub); err != nil {
		return false
	}
	return true
}

// The following functions (with minor modifications) were taken from
// https://gist.github.com/miguelmota/3ea9286bd1d3c2a985b67cac4ba2130a

// bytesToPrivateKey bytes to private key
func bytesToPrivateKey(priv []byte) (
	*rsa.PrivateKey,
	error,
) {
	block, _ := pem.Decode(priv)
	if block == nil || block.Type != "RSA PRIVATE KEY" {
		return nil, errors.New("failed to decode PEM block containing private key")
	}
	key, err := x509.ParsePKCS1PrivateKey(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("while x509.ParsePKCS1PrivateKey in bytesToPrivateKey: %w", err)
	}
	return key, nil
}

// bytesToPublicKey bytes to public key
func bytesToPublicKey(pub []byte) (
	*rsa.PublicKey,
	error,
) {
	block, _ := pem.Decode(pub)
	if block == nil || block.Type != "RSA PUBLIC KEY" {
		return nil, errors.New("failed to decode PEM block containing public key")
	}
	ifc, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		return nil, fmt.Errorf("while x509.ParsePKIXPublicKey in bytesToPublicKey: %w", err)
	}
	key, ok := ifc.(*rsa.PublicKey)
	if !ok {
		return nil, errors.New("public key not ok")
	}
	return key, nil
}

// encryptWithPublicKey encrypts data with public key
func encryptWithPublicKey(msg []byte, pub *rsa.PublicKey) (
	[]byte,
	error,
) {
	hash := sha256.New()
	ciphertext, err := rsa.EncryptOAEP(hash, rand.Reader, pub, msg, nil)
	if err != nil {
		return nil, fmt.Errorf("while rsa.EncryptOAEP in encryptWithPublicKey: %w", err)
	}
	return ciphertext, nil
}

// decryptWithPrivateKey decrypts data with private key
func decryptWithPrivateKey(ciphertext []byte, priv *rsa.PrivateKey) (
	[]byte,
	error,
) {
	hash := sha256.New()
	plaintext, err := rsa.DecryptOAEP(hash, rand.Reader, priv, ciphertext, nil)
	if err != nil {
		return nil, fmt.Errorf("while rsa.DecryptOAEP in decryptWithPrivateKey: %w", err)
	}
	return plaintext, nil
}

//signWithPrivateKey signs message with private key
func signWithPrivateKey(msg []byte, priv *rsa.PrivateKey) (
	[]byte,
	error,
) {
	hashed := sha256.Sum256(msg)

	signature, err := rsa.SignPKCS1v15(rand.Reader, priv, crypto.SHA256, hashed[:])
	if err != nil {
		return nil, fmt.Errorf("while rsa.SignPKCS1v15 in signWithPrivateKey: %w", err)
	}

	return signature, nil
}

//verifySignatureWithExternalPublicKey verifies signature with the provided public key
func verifySignatureWithExternalPublicKey(
	msg []byte,
	signature []byte, //decoded from base64
	externalPub *rsa.PublicKey,
) error {
	hashed := sha256.Sum256(msg)

	err := rsa.VerifyPKCS1v15(externalPub, crypto.SHA256, hashed[:], signature)
	if err != nil {
		return fmt.Errorf("while rsa.VerifyPKCS1v15 in verifySignatureWithExternalPublicKey: %w", err)
	}
	return nil
}
